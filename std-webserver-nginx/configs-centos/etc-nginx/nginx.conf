#
# Nginx master config
#

user                            nginx;
worker_processes                2;                             # number of cpu/cores
worker_rlimit_nofile            262144;
pid                             /run/nginx.pid;

events {
    # Determines how many clients will be served by each worker process.
    # (Max clients = worker_connections * worker_processes)
    # "Max clients" is also limited by the number of socket connections available on the system (~64k)
    worker_connections          32768;

    # essential for linux, optmized to serve many clients with each thread
    use                         epoll;

    # Accept as many connections as possible, after nginx gets notification about a new connection.
    # May flood worker_connections, if that option is set too low.
    multi_accept                on;
}

http {

    # Default charset
    charset                     utf-8;

    # Sendfile copies data between one FD and other from within the kernel. 
    # More efficient than read() + write(), since the requires transferring data to and from the user space.
    sendfile                    on;

    # Tcp_nopush causes nginx to attempt to send its HTTP response head in one packet, 
    # instead of using partial frames. This is useful for prepending headers before calling sendfile, 
    # or for throughput optimization.
    tcp_nopush                  on;

    # don't buffer data-sends (disable Nagle algorithm). Good for sending frequent small bursts of data in real time.
    tcp_nodelay                 on;

    # don't display who we are
    server_tokens               off;

    # Timeout for keep-alive connections. Server will close connections after this time.
    keepalive_timeout           45 20;

    # Number of requests a client can make over the keep-alive connection
    keepalive_requests          1000;

    # allow the server to close the connection after a client stops responding. Frees up socket-associated memory.
    reset_timedout_connection   on;

    client_header_timeout       3m;
    client_body_timeout         3m;
    send_timeout                3m;

    client_max_body_size        1280m;
    connection_pool_size        256;
    client_body_buffer_size     128k;
    client_header_buffer_size   1k;
    large_client_header_buffers 4 4k;

    # Caches information about open FDs, freqently accessed files.
    open_file_cache             max=10000 inactive=30s;
    open_file_cache_valid       60s;
    open_file_cache_min_uses    2;
    open_file_cache_errors      on;

    include                     /etc/nginx/mime.types;
    default_type                application/octet-stream;

    # Buffer log writes to speed up IO, or disable them altogether
    #access_log /var/log/nginx/access.log main buffer=16k;
    access_log                  off;
    error_log                   /var/log/nginx/error.log;

    # Proxy-Cacheing: Access log format for debugging cache hits
    log_format cache_log        '$remote_addr - $upstream_cache_status [$time_local]  '
                                '"$request" $status $body_bytes_sent '
                                '"$http_referer" "$http_user_agent"';

    # Compression. Reduces the amount of data that needs to be transferred over the network
    gzip                        on;
    gzip_static                 on;
    gzip_vary                   on;
    gzip_buffers                16 8k;
    gzip_comp_level             5;
    gzip_min_length             1400;
    gzip_proxied                any;
    gzip_disable                "MSIE [1-6]\.(?!.*SV1)";
    gzip_types                  text/plain
                                text/css
                                text/xml
                                text/xsd
                                text/xsl
                                text/richtext
                                text/x-component
                                text/x-js
                                text/javascript
                                application/json
                                application/x-javascript
                                application/javascript
                                application/xml
                                application/xml+rss
                                image/svg+xml
                                image/x-icon;


    # SSL (see: https://mozilla.github.io/server-side-tls/ssl-config-generator/)
    ssl_session_timeout         1d;
    ssl_session_cache           builtin:1000 shared:SSL:50m;
    ssl_session_tickets         off;
    ssl_prefer_server_ciphers   on;
    ssl_protocols               TLSv1 TLSv1.1 TLSv1.2;
    ssl_dhparam                 /etc/nginx/dhparam.pem;
    ssl_ciphers                 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:ECDHE-RSA-DES-CBC3-SHA:ECDHE-ECDSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA';

    # Proxy
    proxy_ignore_client_abort   on;
    proxy_set_header            Host $host;
    proxy_set_header            X-Real-IP $remote_addr;
    proxy_set_header            X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header            X-Forwarded-Proto $scheme;
    proxy_send_timeout          120;
    proxy_read_timeout          205;
    proxy_connect_timeout       74;
    proxy_buffers               16 64k;
    proxy_buffer_size           128k;
    proxy_busy_buffers_size     256k;
    proxy_temp_file_write_size  256k;
    proxy_http_version          1.1;

    # PHP
    fastcgi_intercept_errors        on;
    fastcgi_ignore_client_abort     on;
    fastcgi_buffers                 256 16k;
    fastcgi_buffer_size             128k;
    fastcgi_busy_buffers_size       256k;
    fastcgi_temp_file_write_size    256k;
    fastcgi_read_timeout            360;
    fastcgi_send_timeout            360;
    fastcgi_connect_timeout         1200;
    fastcgi_index                   index.php;
    fastcgi_hide_header             X-Powered-By;

    fastcgi_cache_min_uses          1;
    fastcgi_cache_valid             200 302 30m;
    fastcgi_cache_valid             404 5m;
    fastcgi_cache_use_stale         error timeout invalid_header updating http_500 http_503;
    fastcgi_cache_key               "$scheme$request_method$host$request_uri";
    fastcgi_ignore_headers          "X-Accel-Redirect" "X-Accel-Expires" "Expires" "Cache-Control" "Set-Cookie";

    # If you do not explicitly set underscores_in_headers on;, nginx will silently drop HTTP 
    # headers with underscores (which are perfectly valid according to the HTTP standard). 
    # This is done in order to prevent ambiguities when mapping headers to CGI variables, as 
    # both dashes and underscores are mapped to underscores during that process.
#    underscores_in_headers on;

    # Enable this to prevent ddos
#    include /etc/nginx/conf.d/ddos-prevention.conf

    # Global Maps for all sites
    include /etc/nginx/conf.d/global-maps.conf;

    # Virtual Host Configs
    include /etc/nginx/sites-enabled/*.conf;
}

# eof
